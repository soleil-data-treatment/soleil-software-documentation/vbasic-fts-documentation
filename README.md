# Aide et ressources de VBasic-FTS pour Synchrotron SOLEIL

## Résumé

- Acquisition et calcul des spectres.
- Créé à Synchrotron Soleil

## Sources

- Code source: Non, pas de distribution
- Documentation officielle: Oui

## Installation

- Systèmes d'exploitation supportés: Windows
- Installation: En général les utilisateurs exploitent les fichiers txt produits par VB

## Format de données

- en entrée: Les données sont enregistrées sous un format binaire
- en sortie: Les données sont exportées vers un simple fichier ascii
- sur un disque dur, sur la Ruche
